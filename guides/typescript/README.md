# Typescript styleguide

### Синтаксис
По умолчанию используется `snake_case` для написания переменных и методов, а также параметров передаваемых в функции, конструкторы классов.

Названия классов, интерфейсов, енамов и в целом типов всегда пишутся с большой буквы `CamelCase`.

Если в проекте используется rxjs то, переменные на которые можно подписаться (которые возвращают observable) должны писаться с `snake_case$` с `$` на конце.

Константы общего пользования пишутся капсом `SNACK_CASE`.
### Наименования
Во всех случаях нужно создавать переменные и методы с "говорящими" названиями, которые полностью описывают для чего нужна это нужно. При этом сокращение названий уместно. Это может быть аббревиатура понятная для всех, либо просто сокращенное слово. Очень важно, чтобы все понимали с первого взгляда для чего нужен тот или иной метод или переменная.

**Плохо**
```typescript
const time = setInterval(() => {...}, 86400000);
// Если где-то в коде встретится clearTimeout(time), будет не понятно что это за таймер. Второй момент, время, которое указано на таймере также не понятно. За секунду понять что 86400000 это 1 день - сложно.
```

**Хорошо**
```typescript
const MILLISECONDS_IN_A_DAY = 60 * 60 * 24 * 1000;
const health_check_timer = setInterval(() => {...}, MILLISECONDS_IN_A_DAY);
```
****

### Параметры
Входные параметры в методы функции и конструкторы классов должны быть явными и понятными для восприятия, ровно также как описано в блоке наименований.
**Плохо**
```typescript
const convert_color(color: string): string {
  // do some logic
  return converted_color;
}
/* Не понятно что, откуда и куда конвертится. 
  Слово color во входных параметрах тоже не помогает понять о чем это метод.
  Более того, если метод большой, то при виде color можно забыть что представляет из себя этот входной параметр.

```

**Плохо**
```typescript
const convert_color_to_hue(color: string): string {
  // do some logic
  return converted_color;
}
// В данном пример понятно куда конвертится, но непонятно откуда
```

**Хорошо**
```typescript
const convert_color_rgb_to_hue(rgb_color: string): string {
  // do some logic
  return hue_color;
}
/* 
  В данном пример понятно откуда и куда, и внутри метода сложно будет запутстся что есть rgb, а что hue. 
  Все указано в явном, понятном виде.
*/
```
****

### Условия
Нужно стараться избегать негативных условий

**Плохо**
```typescript
const visible = false;

if (!visible) {
  // bad
} else {

}
```

**Хорошо**
```typescript
const visible = false;

if (visible) {
  // good
} else {

}
```
****

Нужно избегать больших цепочек if-else. Такие цепочки делают код запустанным и непонятным. Если логики много, то в таких ситуациях лучше разделить метод на несколько раздельных, и использовать switch-case, либо если логики мало, то написать ее прямо в case'е.

**Плохо**
```typescript
const visible = false;

if (color === Color.Green) {
  // bad
} else if(color === Color.Red {

} else if (...) {

} else {

}
```

**Хорошо**
```typescript
const visible = false;

switch (color) {
  case Color.Green: {
    do_some_stuff_with_green_color();
    break;
  }
  case Color.Red: {
    do_some_stuff_with_red_color();
    break;
  }
  default: {
    console.warn(`Color ${color} not found`);
    break;
  }
```
****
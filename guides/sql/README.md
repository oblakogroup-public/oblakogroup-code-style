# SQL Style Guide

## Используйте UPPERCASE для ключевых слов

```sql
/* ПЛОХО */
select * from users;

/* ХОРОШО */
SELECT * FROM users;
```

## Если требуется более двух колонок, то переносите запрос на несколько строк

```sql
/* ПЛОХО */
SELECT id, fname, lname, mname FROM users;

/* ХОРОШО */
SELECT
  id,
  fname,
  lname,
  mname
FROM users;
```

```sql
/* ПЛОХО */
SELECT jsonb_agg(jsonb_build_object('id', id, 'fname', fname, 'lname', lname, 'mname', mname)) AS user_arr FROM users;

/* ХОРОШО */
SELECT
  jsonb_agg(
    jsonb_build_object(
      'id', id,
      'fname', fname,
      'lname', lname,
      'mname', mname
    )
  ) AS user_arr
FROM users;
```

## Разбивайте конструкции на несколько строк

```sql
/* ПЛОХО */
SELECT * FROM users WHERE age > 25 AND lname ILIKE '%username%'

/* ХОРОШО */
SELECT * FROM users
WHERE age > 25
  AND lname ILIKE '%username%';
```

```sql
/* ПЛОХО */
SELECT * FROM devices INNER JOIN users ON users.id = devices.user_id AND devices.type = 'pc';

/* ХОРОШО */
SELECT * FROM devices
INNER JOIN users
  ON users.id = devices.user_id
  AND devices.type = 'pc';
```

## Старайтесь не использовать условия в JOIN конструкциях

```sql
/* ПЛОХО */
SELECT * FROM devices
INNER JOIN users
  ON users.id = devices.user_id
  AND devices.type = 'pc'; /* ! */

/* ХОРОШО */
SELECT * FROM devices
INNER JOIN users
  ON users.id = devices.user_id
WHERE devices.type = 'pc'; /* ! */
```

## Аггрегированные поля должны быть снизу

```sql
/* ПЛОХО */
SELECT
  COUNT(devices.id) AS count, /* ! */
  user_users.id
FROM users
LEFT JOIN devices
  ON devices.user_id = users.id
GROUP BY users.id;

/* ХОРОШО */
SELECT
  user_users.id,
  COUNT(devices.id) AS count /* ! */
FROM users
LEFT JOIN devices
  ON devices.user_id = users.id
GROUP BY users.id;
```

## Не называйте колонки зарезервированными словами

```sql
/* ПЛОХО */
SELECT
  user_users.id,
  COUNT(devices.id) AS count /* ! */
FROM users
LEFT JOIN devices
  ON devices.user_id = users.id
GROUP BY users.id;

/* ХОРОШО */
SELECT
  user_users.id,
  COUNT(devices.id) AS devices_count /* ! */
FROM users
LEFT JOIN devices
  ON devices.user_id = users.id
GROUP BY users.id;
```

## Не давайте имена таблицам - используйте оригинальные

```sql
/* ПЛОХО */
SELECT
  u.id,
  u.lname,
  u.fname
FROM users u /* ! */
INNER JOIN devices d
  ON d.user_id = u.id

/* ХОРОШО */
SELECT
  users.id,
  users.lname,
  users.fname
FROM users /* ! */
INNER JOIN devices
  ON devices.user_id = users.id

```

## Используйте AS, если дать другое имя необходимо

```sql
/* ПЛОХО */
SELECT
  id,
  lname last_name, /* ! */
  fname
FROM users;

/* ХОРОШО */
SELECT
  id,
  lname AS last_name, /* ! */
  fname
FROM users;
```

## Написание switch-case

```sql
/* ПЛОХО */
SELECT
  id,
  CASE type_id WHEN 1 THEN 'admin' /* ! */
  ELSE 'user' END AS role_type
FROM users;

/* ХОРОШО */
SELECT
  id,
  (
    CASE type_id /* ! */
      WHEN 1 THEN 'admin'
      ELSE 'user'
    END
  ) AS role_type
FROM users;
```

## Используйте WITH для написания повторяющихся или последовательных подзапросов

```sql
/* ПЛОХО */
  SELECT user_id FROM company_a
  WHERE user_id IN (
    SELECT user_id FROM devices /* ! */
    WHERE type_id = 'pc'
  )
UNION ALL
  SELECT user_id FROM company_b
  WHERE user_id IN (
    SELECT user_id FROM devices /* ! */
    WHERE type_id = 'pc'
  )

/* ХОРОШО */
WITH pc_users AS (
  SELECT user_id FROM devices /* ! */
  WHERE type_id = 'pc'
)
  SELECT user_id FROM company_a
  WHERE user_id IN (SELECT user_id FROM pc_users)
UNION ALL
  SELECT user_id FROM company_b
  WHERE user_id IN (SELECT user_id FROM pc_users)
```

```sql
/* ПЛОХО */
SELECT * FROM ( /* ! */
  SELECT * FROM events
  WHERE starts_at IN ( /* ! */
    SELECT g_date
    FROM generate_series(CURRENT_TIMESTANP - interval '14 days', CURRENT_TIMESTAMP, interval '1 days') AS g_date
  )
) AS filtered_events
INNER JOIN locations
  ON filtered_events.location_id = locations.id;

/* ХОРОШО */
WITH search_dates AS ( /* ! */
  SELECT g_date
  FROM generate_series(CURRENT_TIMESTANP - interval '14 days', CURRENT_TIMESTAMP, interval '1 days') AS g_date
), filtered_events AS ( /* ! */
  SELECT * FROM events
  WHERE starts_at IN (SELECT g_date FROM search_dates)
)
SELECT * FROM filtered_events
INNER JOIN locations
  ON filtered_events.location_id = locations.id;
```

## Не переносите скобки на следующие строки там, где это не надо

```sql
/* ПЛОХО */
WITH search_dates AS
(
  SELECT g_date
  FROM generate_series(CURRENT_TIMESTANP - interval '14 days', CURRENT_TIMESTAMP, interval '1 days') AS g_date
)
SELECT * FROM search_dates
```

```sql
/* ХОРОШО */
WITH search_dates AS (
  SELECT g_date
  FROM generate_series(CURRENT_TIMESTANP - interval '14 days', CURRENT_TIMESTAMP, interval '1 days') AS g_date
)
SELECT * FROM search_dates
```

## Явно указывайте колонки во всех запросах

```sql
/* ПЛОХО */
WITH search_dates AS (
  SELECT g_date
  FROM generate_series(CURRENT_TIMESTANP - interval '14 days', CURRENT_TIMESTAMP, interval '1 days') AS g_date
), filtered_events AS (
  SELECT * FROM events
  WHERE starts_at IN (SELECT g_date FROM search_dates)
)
SELECT * FROM filtered_events /* ! */
INNER JOIN locations
  ON filtered_events.location_id = locations.id;

/* ХОРОШО */
WITH search_dates AS (
  SELECT g_date
  FROM generate_series(CURRENT_TIMESTANP - interval '14 days', CURRENT_TIMESTAMP, interval '1 days') AS g_date
), filtered_events AS (
  SELECT id, title FROM events
  WHERE starts_at IN (SELECT g_date FROM search_dates)
)
SELECT
  filtered_events.id AS event_id, /* ! */
  filtered_events.title AS event_title,
  locations.id AS location_id,
  locations.title AS location_title
FROM filtered_events
INNER JOIN locations
  ON filtered_events.location_id = locations.id;
```

## Ковычки

| Одинарные ковычки для значения переменных

```sql
/* ПЛОХО */
SELECT id FROM users
WHERE lname = "username"; /* ! */
/* ТАК НЕ БУДЕТ РАБОТАТЬ - ЭТИ КОВЫЧКИ НЕ ДЛЯ ЭТОГО */

/* ХОРОШО */
SELECT id FROM users
WHERE lname = 'username'; /* ! */
```

| Двойные ковычки используются для таблиц или колонок

```sql
/* ПЛОХО */
SELECT id FROM users
WHERE isActive = true; /* ! */
/* ТАК НЕ БУДЕТ РАБОТАТЬ ИЗ-ЗА CamelCase */

/* ХОРОШО */
SELECT id FROM users
WHERE "users"."isActive" = true; /* ! */
/* УКАЗАТЬ ПРОСТО КОЛОНКУ В КОВЫЧКАХ НЕЛЬЗЯ - ТАК НЕ БУДЕТ РАБОТЬ */
```

## Чаще читайте документацию

| Мы используем PostgreSQL v9.6+

[PostgreSQL 9.6](https://www.postgresql.org/docs/9.6/index.html)

## Доп. ссылки

* [GitLab's SQL Style Guide](https://about.gitlab.com/handbook/business-ops/data-team/sql-style-guide/)
* [SQL KeyWords](https://www.postgresql.org/docs/9.6/sql-keywords-appendix.html)
  
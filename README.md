# OBLAKO Group Code Style

## Структура

```txt
-- README.md
-- assets
  -- file
-- guides
  -- name
    -- README.md
```

## Markdown

Все те, кто хочет написать гайд должны ознакомится [с этим](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

## Style Guides

* [SQL](guides/sql)
* [Ruby](https://github.com/uohzxela/clean-code-ruby)
* [JavaScript](https://github.com/ryanmcdermott/clean-code-javascript)
* [TypeScript](https://github.com/labs42io/clean-code-typescript)
  